const express = require("express");
const bodyParser = require("body-parser");

// Import module date
const date = require(__dirname + "/date.js");

const app = express();

// Body parser for content type
app.use(bodyParser.urlencoded({ extended: true }));
//
app.use(express.static("public"));

// Render html with view engine ejs
app.set("view engine", "ejs");

// Variable declaration
const items = [];
const workItems = [];

// routing for landing page / home
app.get("/", function (req, res) {
  const day = date.getDate();
  res.render("list", { listTitle: day, newListItems: items });
});

// post form List Item
app.post("/", function (req, res) {
  const item = req.body.newItem;

  if (req.body.list === "Work List") {
    workItems.push(item);
    res.redirect("/work");
  } else {
    items.push(item);
    res.redirect("/");
  }
});

// Routing work page
app.get("/work", function (req, res) {
  res.render("list", { listTitle: "Work List", newListItems: workItems });
});

app.post("/work", function (req, res) {
  const item = req.body.newItem;

  workItems.push(item);

  res.redirect("/work");
});

// Routing for about page
app.get("/about", function (req, res) {
  res.render("about");
});

// Server
app.listen(3000, function () {
  console.log("Server running on port 3000");
});
