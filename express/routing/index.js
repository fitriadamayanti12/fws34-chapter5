const express = require("express");
const app = express();

const logger = (req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
};

app.use(logger);

// Setiap GET request ke htpp://localhost:3000/ diarahkan ke handler ini
app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/products", (req, res) => {
  res.json(["Pen", "Pencil", "Book"]);
});

app.get("/orders", (req, res) => {
  res.json([
    {
      id: 1,
      paid: false,
      user_id: 1,
    },
    {
      id: 2,
      paid: false,
      user_id: 1,
    },
  ]);
});

app.listen(3000, () => {
  console.log("Server running on port 3000");
});
