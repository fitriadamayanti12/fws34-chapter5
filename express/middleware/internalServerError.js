const express = require("express");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const router = require("./router");
app.get("/iniError", (req, res) => {
  iniError;
});
app.use(router);

// Internal server error handler
app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

// 404 Handler
app.use(function (req, res, next) {
  res.status(404).json({
    status: "fail",
    errorrs: "are you lost?",
  });
});

app.get("/endpointLain", (req, res) => {
  res.send("Lain");
});

app.listen(3000, () => {
  console.log("Server running");
});
