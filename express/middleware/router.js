const express = require("express");
const router = express.Router();

// Middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

// Define the home page router
router.get("/", function (req, res) {
  res.send("Pets home page");
});

// Define the about route
router.get("/about", function (req, res) {
  res.send("About pets");
});

module.exports = router;
