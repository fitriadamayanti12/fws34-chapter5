const express = require("express");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const router = require("./router");
app.use(router);

app.get("/endpointLain", (req, res) => {
  res.send("Lain");
});

app.listen(3000, () => {
  console.log("Server running");
});
