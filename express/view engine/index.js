const express = require("express");
const app = express();
const users = [];

// Set middlware view engine in folder views
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));

// Method get for jumlah user dari index.ejs
app.get("/", (req, res) => {
  res.send(`Jumlah user ${users.length}`);
  //res.render("index");
});

// Method get untuk endpoint /greet untuk halaman greet
// http://localhost:3000/greet?name=Fikri
app.get("/greet", (req, res) => {
  const name = req.query.name || "Void";
  res.render("greet", {
    name,
  });
});

// Method get untuk endpoint register
app.get("/register", (req, res) => {
  res.render("register");
});

// Mehod POST untuk send data from resigter form
app.post("/register", (req, res) => {
  const { email, password } = req.body;

  // Push email & password from user input
  users.push({ email, password });
  res.redirect("/");
});

// Server running
app.listen(3000, () => {
  console.log("Server running");
});
