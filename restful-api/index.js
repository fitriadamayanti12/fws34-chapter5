const express = require("express");
const app = express();
let posts = require("./db/posts.json");

// Read json data
app.use(express.json());

/* 
GET all data
endpoint: http://localhost:3000/api/v1/posts for get all data, method -> GET
*/
app.get("/api/v1/posts", (req, res) => {
  res.status(200).json(posts);
});

/*
GET data by id
endpoint: http://localhost:3000/api/v1/posts/1 for get by id, method -> GET
*/
app.get("/api/v1/posts/:id", (req, res) => {
  const post = posts.find((i) => i.id === +req.params.id);
  res.status(200).json(post);
});

// POST
app.post("/api/v1/posts", (req, res) => {
  /**
   * Menghandle request body
   * Call req.body
   */
  const { title, author, price, stock } = req.body;

  // GET id
  const id = posts[posts.length - 1].id + 1;
  const post = {
    id,
    title,
    author,
    price,
    stock,
  };

  // Save data in array
  posts.push(post);
  res.status(201).json(post);
});

/**
 * PUT
 * endpoint: http://localhost:3000/api/v1/posts/1 for post a data -> method PUT
 */
app.put("/api/v1/posts/:id", (req, res) => {
  let post = posts.find((i) => i.id === +req.params.id);

  const params = {
    title: req.body.title,
    author: req.body.author,
    price: req.body.price,
    stock: req.body.stock,
  };

  post = { ...post, ...params };

  post = posts.map((i) => (i.id === post.id ? post : i));

  res.status(200).json(post);
});

/**
 * DELETE
 * endpoint: http://localhost:3000/api/v1/posts/1 for delete, use DELETE Method
 */
app.delete("/api/v1/posts/:id", (req, res) => {
  posts = posts.filter((i) => i.id !== +req.params.id);

  res.status(200).json({
    message: `Post dengan id ${req.params.id} sudah berhasil dihapus`,
  });
});

// Server
app.listen(3000, () => {
  console.log("Server running on port 3000");
});
