/* 
1. Use the inquirer npm package to get user input --> https://www.npmjs.com/package/inquirer (install)
2. Use the qr-image npm package to turn the user entered URL into a QR code image --> https://www.npmjs.com/package/qr-image (install)
3. Create a txt file to save the user input using the native fs node module.
4. Di atas dependencies add "type": "module",
5. format name_task5 (branch baru)
*/

import inquirer from "inquirer";
import qr from "qr-image";
import fs from "fs";

/* 
Your code here
*/
