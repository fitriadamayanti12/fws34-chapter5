// Import core module from node.js
const https = require("https");
const fs = require("fs");

const options = {
  key: fs.readFileSync("./key.pem"),
  cert: fs.readFileSync("./cert.pem"),
};
https
  .createServer(options, function (request, response) {
    response.end("Hello World, Mari belajar!");
  })
  .listen(8000);

/**
 * openssl genrsa 1024 > key.pem
 * openssl req -x509 -new -key key.pem > cert.pem
 *
 * node index.js (terminal 1)
 * curl https://localhost:8000 -k (terminal 2)
 * Output: Hello world!
 * -i = information
 * -h =
 * -k = key
 */
